#ifndef GAMEOFLIFE_H
#define GAMEOFLIFE_H

#include <stdint.h>
#include <QPen>
#include <QImage>
#include <QWidget>


class gameOfLife
{
public:
    gameOfLife();

    struct gridSize
    {
        unsigned long x;
        unsigned long y;
        gridSize(){x=800; y=600;}
    } gridSize;

    void init();
    int evaluateCell(unsigned long posX, unsigned long posY);
    void paint(QPainter *painter, QPaintEvent *event);
protected:
    std::vector< std::vector <int> > grid;
private:
    int counter=0;

};

#endif // GAMEOFLIFE_H
