#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"
#include "gameoflife.h"
#include <QTimer>


class MainWindow : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    QTimer *timer = new QTimer(this);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    gameOfLife golptr;
};

#endif // MAINWINDOW_H
