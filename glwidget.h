#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QOpenGLWidget>
#include "gameoflife.h"

class gameOfLife;

class GLWidget : public QOpenGLWidget
{
    Q_OBJECT

public:
    GLWidget(gameOfLife *g, QWidget *parent);

public slots:
    void animate();

protected:
    void paintEvent(QPaintEvent *event) override;

private:
    gameOfLife *g;
    int elapsed;
};

#endif // GLWIDGET_H
