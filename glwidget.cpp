#include "glwidget.h"
#include "gameoflife.h"

#include <QPainter>
#include <QTimer>

GLWidget::GLWidget(gameOfLife *g, QWidget *parent)
    : QOpenGLWidget(parent), g(g)
{
    elapsed = 0;
    setFixedSize(g->gridSize.x, g->gridSize.y);
    setAutoFillBackground(false);

    glEnable(GL_MULTISAMPLE);

}

void GLWidget::animate()
{
    //elapsed = (elapsed + qobject_cast<QTimer*>(sender())->interval()) % 1000;
    update();
}

void GLWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter;
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing);
    g->paint(&painter, event);
    painter.end();
}

