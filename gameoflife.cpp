#include "gameoflife.h"
#include <QPainter>
#include <QPaintEvent>
#include <QWidget>
#include <random>


gameOfLife::gameOfLife()
{
    std::vector< std::vector <int> > aux (gridSize.x, std::vector <int>(gridSize.y, 0));
    grid = aux;

    init();
}

void gameOfLife::init()
{
    std::random_device r;
    std::seed_seq seed{r()};
    std::mt19937 gen(seed);
    std::uniform_int_distribution<uint32_t> dis(0,1);

    for (auto i = 0ul; i < grid.size(); ++i)
    {
        for (auto j = 0ul; j < grid[0].size(); ++j)
        {
            grid[i][j] = static_cast<int>( dis(gen) );
        }
    }
}

int gameOfLife::evaluateCell(unsigned long posX, unsigned long posY)
{
    auto x = gridSize.x; auto y = gridSize.y;

    auto sum = grid[(posX+x-1)%x][(posY+y+1)%y] + grid[posX][(posY+y+1)%y] + grid[(posX+x+1)%x][(posY+y+1)%y] +
               grid[(posX+x-1)%x][posY        ] +           0              + grid[(posX+x+1)%x][posY        ] +
               grid[(posX+x-1)%x][(posY+y-1)%y] + grid[posX][(posY+y-1)%y] + grid[(posX+x+1)%x][(posY+y-1)%y];

    int test = ((grid[posX][posY]==1) && (sum > 1) && (sum < 4)) || ((grid[posX][posY]==0) && (sum == 3));

    return test;
}

void gameOfLife::paint(QPainter *painter, QPaintEvent *event)
{
    painter->fillRect(event->rect(), Qt::black);

    const QSize sz(gridSize.x, gridSize.y);

    QImage image(sz, QImage::Format_RGB32);
    image.fill( QColor( Qt::black ).rgb() );

    std::vector< std::vector <int> > aux (gridSize.x, std::vector <int>(gridSize.y, 0));

    for (auto i = 0ul; i < grid.size(); ++i)
    {
        for (auto j = 0ul; j < grid[0].size(); ++j)
        {
            aux[i][j] = evaluateCell(i,j);
            image.setPixel(i,j, qRgb(0,255*aux[i][j],0) );
        }
    }

    painter->drawImage(event->rect(), image);

    grid.clear();
    grid = aux;
}


