#include "mainwindow.h"
#include "glwidget.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    setupUi(this);

    GLWidget *openGL = new GLWidget(&golptr, this);
    openGL->setGeometry(10,40,800,600);

    connect(timer, &QTimer::timeout, openGL, &GLWidget::animate);

//    openGL->installEventFilter(this);
}

void MainWindow::on_pushButton_clicked()
{
    timer->start(50);
}

void MainWindow::on_pushButton_2_clicked()
{
    timer->stop();
}


